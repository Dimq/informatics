#include "stdio.h"
#include "conio.h"
#include "windows.h"

#define HERO_WIDTH 10
#define HERO_HEIGHT 14
#define HERO_ACTION_COUNT 2
#define HERO_SPRITES_COUNT 2

#define HERO_RUN_RIGHT 0
#define HERO_RUN_LEFT 1

#define BORDER_LEFT 10
#define BORDER_RIGHT 70
#define BORDER_DOWN 24


HANDLE hStdOut;

void gotoxy(int x, int y)
{
	COORD c = { x, y };
	SetConsoleCursorPosition(hStdOut, c);
}

void cursorHide()
{
	CONSOLE_CURSOR_INFO cursorInfo;
	cursorInfo.dwSize = 100;
	cursorInfo.bVisible = FALSE;

	SetConsoleCursorInfo(hStdOut,&cursorInfo);
}




int Hero[HERO_ACTION_COUNT][HERO_SPRITES_COUNT][HERO_HEIGHT][HERO_WIDTH] = {
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,0,1,1,0,0,0,1,
	0,0,1,1,1,1,1,0,0,1,
	0,1,0,1,1,1,0,1,1,0,
	0,1,0,0,1,1,0,0,0,0,
	0,1,0,0,1,1,0,0,0,0,
	0,0,1,0,1,1,1,0,0,0,
	0,0,0,0,1,1,1,1,0,0,
	0,0,0,1,1,0,0,1,1,0,
	0,0,0,1,1,0,0,0,1,1,
	0,0,0,1,1,0,0,1,1,0,
	0,0,1,1,0,0,0,1,1,0,
	0,0,1,1,0,0,0,0,0,0,

	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,1,1,1,1,0,0,0,
	0,0,1,1,1,1,1,0,0,0,
	0,0,1,1,1,1,1,1,1,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,0,1,1,1,0,0,0,
	0,0,0,1,1,1,1,1,0,0,
	0,1,1,1,1,0,1,1,0,0,
	0,1,1,0,0,0,1,1,0,0,
	0,0,0,0,0,1,1,0,0,0,
	0,0,0,0,0,1,1,0,0,0,

	//-----------------------------------

	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,1,1,0,0,0,0,
	1,0,0,0,1,1,0,0,0,0,
	1,0,0,1,1,1,1,1,0,0,
	0,1,1,0,1,1,0,1,1,0,
	0,0,0,0,1,1,0,0,1,0,
	0,0,0,0,1,1,0,0,1,0,
	0,0,0,1,1,1,0,1,0,0,
	0,0,1,1,1,1,0,0,0,0,
	0,1,1,0,0,1,1,0,0,0,
	1,1,0,0,0,1,1,0,0,0,
	0,1,1,0,0,1,1,0,0,0,
	0,1,1,0,0,0,1,1,0,0,
	0,0,0,0,0,0,1,1,0,0,

	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,1,1,1,1,0,0,0,
	0,0,0,1,1,1,1,1,0,0,
	0,1,1,1,1,1,1,1,0,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,0,1,1,0,0,0,0,
	0,0,0,1,1,1,0,0,0,0,
	0,0,1,1,1,1,1,0,0,0,
	0,0,1,1,0,1,1,1,1,0,
	0,0,1,1,0,0,0,1,1,0,
	0,0,0,1,1,0,0,0,0,0,
	0,0,0,1,1,0,0,0,0,0
};

void DrawHero(int action, int hs, int x, int y);
void EraseHero(int x, int y);
void DrawHeroRun(int x, int y, int dx);
void DrawEnv();

void main()
{
	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	cursorHide();
	
	int hero_x = 11, hero_y = 10, hero_dx = 3;
	DrawEnv();
	while (!_kbhit())
	{
		//�������� ��������� � ������� �����������. ��� �� ����� ����������� �� ���������� �����.
		DrawHeroRun(hero_x, hero_y, hero_dx);
		
		//�������� ��������� ����� ����������---------------------------------------------------------------------
		//���� �������� � ������� ����������� ��������� ����, ������� ����������� �������� �� ���������������.
		if (hero_x + hero_dx < BORDER_LEFT || hero_x + HERO_WIDTH + hero_dx > BORDER_RIGHT)
		{
			hero_dx = -hero_dx;
		}
		//�������� ��������� ����������.
		hero_x += hero_dx;
		//--------------------------------------------------------------------------------------------------------

		//���������� �����.
		Sleep(400);
	}

	_getch();
}

void DrawHeroRun(int x, int y, int dx)
{
	static int i=0;
	int action;

	if (0==dx)
	{
		return;
	}

	if (dx > 0)
	{
		action = HERO_RUN_RIGHT;
	}
	else
	{
		action = HERO_RUN_LEFT;
	}

	if (i > 0)
	{
		EraseHero(x-dx, y);
	}

	DrawHero(action, i % HERO_SPRITES_COUNT, x,y);
	i++;
}


void DrawHero(int action, int hs, int x, int y)
{
	int i, j;

	for (i = 0; i < HERO_HEIGHT; i++)
	{
		for (j = 0; j < HERO_WIDTH; j++)
		{	
			if (Hero[action][hs][i][j] == 1)
			{
				gotoxy(x + j, y + i);
				printf("#");
			}
		}
	}
}

void EraseHero(int x, int y)
{
	int i, j;

	for (i = 0; i < HERO_HEIGHT; i++)
	{
		for (j = 0; j < HERO_WIDTH; j++)
		{
			gotoxy(x + j, y + i);
			printf(" ");
		}
	}
}

void DrawEnv()
{
	int i;

	for (i = BORDER_LEFT; i < BORDER_RIGHT; i++)
	{
		gotoxy(i, BORDER_DOWN);
		printf("=");
	}
}