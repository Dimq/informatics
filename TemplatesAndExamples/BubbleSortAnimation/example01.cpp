#include "stdio.h"
#include "conio.h"
#include "windows.h"

#define N 10
#define DEL 500

HANDLE hStdOut;

enum colors_enum
{
	Black = 0,
	Grey = FOREGROUND_INTENSITY,
	LightGrey = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
	White = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	Blue = FOREGROUND_BLUE,
	Green = FOREGROUND_GREEN,
	Cyan = FOREGROUND_GREEN | FOREGROUND_BLUE,
	Red = FOREGROUND_RED,
	Purple = FOREGROUND_RED | FOREGROUND_BLUE,
	LightBlue = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	LightGreen = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
	LightCyan = FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	LightRed = FOREGROUND_RED | FOREGROUND_INTENSITY,
	LightPurple = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	Orange = FOREGROUND_RED | FOREGROUND_GREEN,
	Yellow = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
};

void gotoxy(int x, int y)
{
	COORD c = { x, y };
	SetConsoleCursorPosition(hStdOut, c);
}

void setcolor(WORD attr)
{
	SetConsoleTextAttribute(hStdOut, attr);
}


void BubbleSort(int A[], int sz);
void Show(int A[], int sz);

void ShowExt(int A[], int sz, WORD arr_col, int p1, int p2, WORD elem_col, int dy,int p3, WORD sort_col);


void main()
{
	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	int Arr[N] = { 12,34,6,3,67,9,4,32,7,9 };

	//setcolor(Blue);
	//gotoxy(3, 3);
	//Sleep(1000);
	//_kbhit()
	//_getch();

	//Show(Arr, N);
	BubbleSort(Arr, N);
	ShowExt(Arr, N, White, -1, -1, White, 0,0,LightBlue);

	
	_getch();
}

void BubbleSort(int A[], int sz)
{
	int i, j;
	int tmp;

	for (i = 0; i < sz - 1; i++)
	{
		for (j = 1; j < sz-i; j++)
		{
			ShowExt(A, N, White, j-1, j, Yellow, 0, sz-i, LightBlue);
			Sleep(DEL);
			ShowExt(A, N, Black, j - 1, j, Black, 0, sz - i, LightBlue);
			ShowExt(A, N, White, j - 1, j, Yellow, -1, sz - i, LightBlue);
			Sleep(DEL);
			ShowExt(A, N, Black, j - 1, j, Black, -1, sz - i, LightBlue);
			if (A[j - 1] > A[j])
			{
				ShowExt(A, N, White, j - 1, j, Green, -1, sz - i, LightBlue);
				Sleep(DEL);
				ShowExt(A, N, Black, j - 1, j, Black, -1, sz - i, LightBlue);
				tmp = A[j - 1];
				A[j - 1] = A[j];
				A[j] = tmp;
				ShowExt(A, N, White, j - 1, j, Green, -1, sz - i, LightBlue);
				Sleep(DEL);
				ShowExt(A, N, Black, j - 1, j, Black, -1, sz - i, LightBlue);
				ShowExt(A, N, White, j - 1, j, Green, 0, sz - i, LightBlue);
				Sleep(DEL);
				ShowExt(A, N, Black, j - 1, j, Black, 0, sz - i, LightBlue);
			}
			else
			{
				ShowExt(A, N, White, j - 1, j, Yellow, 0, sz - i, LightBlue);
				Sleep(DEL);
				ShowExt(A, N, Black, j - 1, j, Black, 0, sz - i, LightBlue);
			}
			ShowExt(A, N, White, j - 1, j, White, 0, sz - i, LightBlue);
			Sleep(DEL);
			ShowExt(A, N, Black, j - 1, j, Black, 0, sz - i, LightBlue);
			if (_kbhit())
			{
				_getch();
				return;
			}
		}
	}
}

void Show(int A[], int sz)
{
	int i;

	//gotoxy(5, 13);
	for (i = 0; i < sz; i++)
	{
		printf("%d ", A[i]);
	}
	printf("\n");
}

void ShowExt(int A[], int sz, WORD arr_col, int p1, int p2, WORD elem_col, int dy, int p3, WORD sort_col)
{
	int i;
	int sx = 20, sy = 13;
	

	for (i = 0; i < sz; i++)
	{
		if (i >= p3)
		{
			setcolor(sort_col);
			gotoxy(sx + i * 3, sy);
		}
		else
		{
			if (i == p1 || i == p2)
			{
				setcolor(elem_col);
				gotoxy(sx + i * 3, sy + dy);
			}
			else
			{
				setcolor(arr_col);
				gotoxy(sx + i * 3, sy);
			}
		}
		printf("%d", A[i]);
	}
	printf("\n");
}