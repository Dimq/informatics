#include "stdio.h"
#include "conio.h"
#include "stdlib.h"
#include "windows.h"

HANDLE hStdOut;

enum colors_enum
{
	Black = 0,
	Grey = FOREGROUND_INTENSITY,
	LightGrey = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
	White = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	Blue = FOREGROUND_BLUE,
	Green = FOREGROUND_GREEN,
	Cyan = FOREGROUND_GREEN | FOREGROUND_BLUE,
	Red = FOREGROUND_RED,
	Purple = FOREGROUND_RED | FOREGROUND_BLUE,
	LightBlue = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	LightGreen = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
	LightCyan = FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	LightRed = FOREGROUND_RED | FOREGROUND_INTENSITY,
	LightPurple = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	Orange = FOREGROUND_RED | FOREGROUND_GREEN,
	Yellow = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
};

void gotoxy(int x, int y)
{
	COORD c = { x, y };
	SetConsoleCursorPosition(hStdOut, c);
}

void setcolor(WORD attr)
{
	SetConsoleTextAttribute(hStdOut, attr);
}


#define N 10
void show(int A[], int n, int mode, int a, int b, int border);
void fill(int A[], int n);
void bubbleSort(int A[], int n, int &cmp, int &swp);

#define Y 10
#define X 30
#define NUM_WIDTH 3
#define LONG_PAUSE 500
#define SHORT_PAUSE 300

void main()
{
	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	//int Arr[N] = { 3,2,1,5,4 };
	//int Arr[N] = {1,2,3,4,5};
	//int Arr[N] = { 5,4,3,2,1 };
	int Arr[N] = { 2,3,4,5,1 };
	//int Arr[N] = { 2,6,5,9,4,0,3,2,1,8 };
	int cmp, swp;
	fill(Arr, N);
	//show(Arr, N);
	bubbleSort(Arr, N,cmp,swp);
	//show(Arr, N, 1);
	//Sleep(1000);
	//show(Arr, N, 0);
	//printf("cmp:%d swp:%d\n", cmp, swp);
	getch();
}

void fill(int A[], int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		A[i] = rand() % 100;
	}
}
void show(int A[], int n, int mode, int a, int b, int border)
{
	int i;

	
	for (i = 0; i < n; i++)
	{
		if (mode)
		{
			if (i == a || i == b)
			{
				setcolor(Red);
				gotoxy(X + (i*NUM_WIDTH), Y-1);
			}
			else
			{
				if (i >= border)
				{
					setcolor(Green);
				}
				else
				{
					setcolor(White);
				}
				gotoxy(X + (i*NUM_WIDTH), Y);
			}
		}
		else
		{
			if (i == a || i == b)
			{
				setcolor(Black);
				gotoxy(X + (i*NUM_WIDTH), Y - 1);
			}
			else
			{
				setcolor(Black);
				gotoxy(X + (i*NUM_WIDTH), Y);
			}
		}
		
		printf("%d ", A[i]);
	}
	printf("\n");
}
void bubbleSort(int A[], int n, int &cmp, int &swp)
{
	int i, j;
	int tmp;
	int fl;

	cmp = 0;
	swp = 0;
	
	show(A, N, 1, -1, -1,N);
	Sleep(LONG_PAUSE);
	show(A, N, 0, -1, -1,N);
	for (i = 0; i < n-1; i++)
	{
		fl = 1;
		for (j = 1; j < n-i; j++)
		{
			show(A, N, 1,j-1,j,N-i);
			Sleep(SHORT_PAUSE);
			show(A, N, 0, j - 1, j,N-i);
			cmp++;
			if (A[j - 1] > A[j])
			{
				tmp = A[j - 1];
				A[j - 1] = A[j];
				A[j] = tmp;
				swp++;
				fl = 0;
				show(A, N, 1, j - 1, j, N - i);
				Sleep(SHORT_PAUSE);
				show(A, N, 0, j - 1, j, N - i);
			}
			show(A, N, 1,-1,-1, N - i);
			Sleep(SHORT_PAUSE);
			show(A, N, 0,-1,-1, N - i);

		}
		
		if (fl)
		{
			show(A, N, 1, -1, -1,0);
			return;
		}
	}
	show(A, N, 1, -1, -1,0);
}

