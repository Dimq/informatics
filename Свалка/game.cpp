#include "stdio.h"
#include "conio.h"
#include "windows.h"

#define HW 6
#define HH 6
#define HSC 2

int Hero[HSC][HH][HW] = { 0,0,1,0,0,0,
					 1,1,1,1,0,0,
					 0,0,1,0,1,0,
					 0,0,1,0,0,0,
					 0,1,0,1,1,0,
					 0,1,0,0,0,0,
	1,0,1,0,0,0,
	0,1,1,1,1,0,
	0,0,1,0,0,0,
	0,1,1,0,0,0,
	0,1,0,1,0,0,
	0,0,0,1,0,0 };




HANDLE hStdOut;

enum colors_enum
{
	Black = 0,
	Grey = FOREGROUND_INTENSITY,
	LightGrey = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
	White = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	Blue = FOREGROUND_BLUE,
	Green = FOREGROUND_GREEN,
	Cyan = FOREGROUND_GREEN | FOREGROUND_BLUE,
	Red = FOREGROUND_RED,
	Purple = FOREGROUND_RED | FOREGROUND_BLUE,
	LightBlue = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	LightGreen = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
	LightCyan = FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	LightRed = FOREGROUND_RED | FOREGROUND_INTENSITY,
	LightPurple = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	Orange = FOREGROUND_RED | FOREGROUND_GREEN,
	Yellow = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
};

void gotoxy(int x, int y)
{
	COORD c = { x, y };
	SetConsoleCursorPosition(hStdOut, c);
}

void setcolor(WORD attr)
{
	SetConsoleTextAttribute(hStdOut, attr);
}



void ShowHero(int hs, int x, int y, int mode);

void Game()
{
	int i;
	//setcolor(Blue);
	//gotoxy(3, 3);
	//Sleep(1000);
	//_kbhit()
	//_getch();

	int hx = 40;
	int hdx = 1;
	int bl = 10, br = 50;

	i = 0;
	while (!_kbhit())
	{
		ShowHero(i % 2, hx, 10, 1);
		Sleep(300);
		ShowHero(i % 2, hx, 10, 0);

		if (hx + hdx <= bl || hx + HW + hdx >= br)
		{
			hdx = -hdx;
		}

		hx += hdx;
		i++;
	}
}

#define MENU_SIZE 3
#define ESC 27
#define ENTER 13
#define UP 72
#define DOWN 80

char menu[MENU_SIZE][8]= { "GAME","OPTIONS","EXIT" };
void showMenu(int pos);

void main()
{
	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	int pos=0;
	int isExit = 0;
	char ch;

	/*ch = _getch();
	ch = _getch();
	printf("%d", ch);*/
	while (!isExit)
	{
		showMenu(pos);
		ch = _getch();
		switch (ch)
		{
		case UP: pos = (pos + MENU_SIZE - 1) % MENU_SIZE;//pos = pos - 1 < 0 ? MENU_SIZE-1 :pos-1;
			break;
		case DOWN: pos=(pos+1)% MENU_SIZE;
			break;
		case ESC: isExit = 1;
			break;
		case ENTER: switch (pos)
		{
		case 0:Game();
			break;
		case 1:break;
		case 2:isExit = 1;
			break;
		}
			break;
		}
	}
	//_getch();
}
void showMenu(int pos)
{
	int i;

	for (i = 0; i < MENU_SIZE; i++)
	{
		if (pos == i)
		{
			setcolor(Green);
		}
		else
		{
			setcolor(White);
		}
		gotoxy(10, 10 + i);
		printf("%s", menu[i]);
	}
}

void ShowHero(int hs, int x, int y, int mode)
{
	int i, j;

	for (i = 0; i < HH; i++)
	{
		for (j = 0; j < HW; j++)
		{
			gotoxy(x + j, y + i);
			if (mode == 1 && Hero[hs][i][j] == 1 )
			{
				printf("#");
			}
			else
			{
				printf(" ");
			}
		}
	}
}