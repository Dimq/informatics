/*Программа сложения*/
#include <stdio.h>

int main()
{
	int integer1, integer2, sum; /*объявление*/

	printf("Enter first integer:"); /*подсказка*/
	scanf("%d", &integer1); /*прочитать целое число*/
	printf("Enter second integer:"); /*подсказка*/
	scanf("%d", &integer2); /*прочитать целое число*/
	sum = integer1 + integer2; /*присвоить сумму*/
	printf("Sum is %d\n", sum); /*напечатать сумму*/

	return 0; /*код успешного завершения программы*/
}