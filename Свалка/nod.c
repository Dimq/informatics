#include <stdio.h>

#define N 3

int Nod(int n1, int n2)
{
	int k;

	while (1)
	{
		k = n1 % n2;
		if (0 == k)
		{
			break;
		}
		n1 = n2;
		n2 = k;
	}

	return n2;
}


void main()
{
	int i;
	int A[N] = { 8,12,10 };
	int nod;

	nod = A[0];
	for (i = 1; i < N; i++)
	{
		nod = Nod(nod, A[i]);
	}

	printf("NOD:%d", nod);
}