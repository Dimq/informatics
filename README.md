Данный репозитарий содержит файлы исходного кода учебных примеров.

---

## Как скачать файлы

Скачать файлы исходного кода можно тремя основными способами:

1. Скопировав содержимое файла из окна просмотра в браузере; 
2. Скачать в виде zip-архива [тут](https://bitbucket.org/Dimq/informatics/downloads/);
3. Склонировать репозитарий.

---

## Клонирование репозитария

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.
---